// console.log("Hello");



// Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let getCube = 2 ** 3;

// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of 2 is ${getCube}`);

// Create a variable address with a value of an array containing details of an address.
let address = ["Blk 10 Lot 37", "Miami Street", "Town and County Southville", "Binan", "Laguna"];

// Destructure the array and print out a message with the full address using Template Literals.
let [houseNumber, street, village, city, province] = address;

console.log(`I live at ${houseNumber}, ${street}, ${village}, ${city}, ${province}.`);

let animal = {
	name: "Pag-asa",
	animalType: "Philippine Eagle",
	weight: 6,
	length: 102,
}

let {animalType, weight, name, length} = animal;

console.log(`${name} was a ${animalType}. He weighed at ${weight}kg with a measurement of ${length}cm.`);

// Create an array of numbers.
let numbers = [5, 10, 15, 20, 25];

// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach(function(numberArray){
	console.log(numberArray);
})

// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
let reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);

console.log(reduceNumber);

// Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog {

	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// Create/instantiate a new object from the class Dog and console log the object.
let myDog = new Dog("Brownie", 2, "Askal");
console.log(myDog);